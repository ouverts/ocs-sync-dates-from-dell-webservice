<?php
// Verbosité du script
define('VERBOSE', true);
define('DEBUG_SQL', false);
define('DEBUG_DATA', false);
define('DEBUG_WS', false);
define('DEBUG_CALC', false);
//define('DEBUG_CALC', '8T9X603');

// Webservice DELL
define('WS_DELL_CLIENT_ID', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
define('WS_DELL_CLIENT_SECRET', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
define('WS_DELL_TOKEN_URL', 'https://apigtwb2c.us.dell.com/auth/oauth/v2/token');
define('WS_DELL_ASSET_ENTITLEMENTS_URL', 'https://apigtwb2c.us.dell.com/PROD/sbil/eapi/v5/asset-entitlements');
define('WS_DELL_PAGINATE_SERVICETAGS', 100);

// Base de données OCS
define('ADM_SERVER_STATIC_DIR', '/usr/share/ocsinventory-reports');
require_once(ADM_SERVER_STATIC_DIR.'/ocsreports/dbconfig.inc.php');
// défini les constantes DB_NAME, SERVER_WRITE, SERVER_PORT, COMPTE_BASE, PSWD_BASE

// Nom des champs personnalisés via l'interface Web d'OCS et sélection des données à updater
define('FIELD_SHIPDATE', 'fields_4');
define('FIELD_WARRANTY_ENDDATE', 'fields_3');
define('SELECT_WHERE_FILTER', "bios.smanufacturer LIKE 'Dell%' AND bios.ssn<>'Precision'");
// AND ( a." . FIELD_SHIPDATE . " is NULL OR a." . FIELD_WARRANTY_ENDDATE . " is NULL )

// Format des ServicesTag Dell
define('REGEX_SERVICETAG', '/^[A-Z0-9]{7}$/');

/* Exemple de retour du webservice dell :
[
  {
    "id": 894912817,
    "serviceTag": "45F9PN2",
    "orderBuid": 909,
    "shipDate": "2018-04-06T00:00:00Z",
    "productCode": "I]002",
    "localChannel": "PUBL",
    "productId": "latitude-12-5280-laptop",
    "productLineDescription": "LATITUDE 5280",
    "productFamily": "all-products/laptops-and-tablets/off-latitude-laptops/latitude-5000-laptops",
    "systemDescription": "Latitude 5280/5288",
    "productLobDescription": "Latitude",
    "countryCode": "FR",
    "duplicated": false,
    "invalid": false,
    "entitlements": [
      {
        "itemNumber": "709-12818",
        "startDate": "2018-04-06T00:00:00Z",
        "endDate": "2019-04-06T04:59:59.999Z",
        "entitlementType": "INITIAL",
        "serviceLevelCode": "CB",
        "serviceLevelDescription": "Collect and Return Support",
        "serviceLevelGroup": 5
      },
      {
        "itemNumber": "865-64957",
        "startDate": "2018-04-06T00:00:00Z",
        "endDate": "2019-04-06T04:59:59.999Z",
        "entitlementType": "INITIAL",
        "serviceLevelCode": "FL",
        "serviceLevelDescription": "ProSupport Flex for Client",
        "serviceLevelGroup": 8
      },
      {
        "itemNumber": "711-10009",
        "startDate": "2018-04-06T00:00:00Z",
        "endDate": "2023-04-06T04:59:59.999Z",
        "entitlementType": "INITIAL",
        "serviceLevelCode": "KH",
        "serviceLevelDescription": "Keep Your Hard Drive/Keep Your Hard Drive for Enterprise Service",
        "serviceLevelGroup": 11
      },
      {
        "itemNumber": "865-64959",
        "startDate": "2018-04-06T00:00:00Z",
        "endDate": "2023-04-06T04:59:59.999Z",
        "entitlementType": "INITIAL",
        "serviceLevelCode": "FL",
        "serviceLevelDescription": "ProSupport Flex for Client",
        "serviceLevelGroup": 8
      }
    ]
  }
]
*/
