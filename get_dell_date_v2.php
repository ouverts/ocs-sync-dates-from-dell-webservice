<?php
/*
 * Copyright 2022 Ludovic Pouzenc <ludovic.pouzenc@univ-smb.fr>, Hamid Makkasse <hamid.makkasse@univ-smb.fr>
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *   the terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 *   A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License along with
 *   this program. If not, see <https://www.gnu.org/licenses/>
 */
include('get_dell_date_config.php');
/* Première requête au webservice pour s'authentifier et avoir l'access token nécessaire pour les requêtes suivantes */
if (VERBOSE) print_verbose(__LINE__, "Demande access_token au webservice");
$access_token = getAccessToken();
if (DEBUG_WS) print_debug(__LINE__, compact('access_token'));

/* Connexion à la base OCS */
$dsn = 'mysql:host='.SERVER_WRITE.';port='.SERVER_PORT.';dbname='.DB_NAME;
if (VERBOSE) print_verbose(__LINE__, "Connexion à la base $dsn");
$dbh = new PDO($dsn, COMPTE_BASE, PSWD_BASE);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Variables liées dans les requêtes
$shipDate = null;
$warrantyEndDate = null;
$hardware_id = null;

/* Préparation de toutes les requêtes SQL (une seule fois) */
$sqlUpdateShipDate="
    UPDATE accountinfo
    SET ".FIELD_SHIPDATE." = :shipDate
    WHERE hardware_id = :hardware_id";
if (DEBUG_SQL) print_debug(__LINE__, compact('sqlUpdateShipDate'));
$sthUpdateShipDate = $dbh->prepare($sqlUpdateShipDate);
$sthUpdateShipDate->bindParam(':shipDate', $shipDate, PDO::PARAM_STR);
$sthUpdateShipDate->bindParam(':hardware_id', $hardware_id, PDO::PARAM_STR);

$sqlUpdateWarrantyEndDate="
    UPDATE accountinfo
    SET ".FIELD_WARRANTY_ENDDATE." = :warrantyEndDate
    WHERE hardware_id = :hardware_id";
if (DEBUG_SQL) print_debug(__LINE__, compact('sqlUpdateWarrantyEndDate'));
$sthUpdateWarrantyEndDate = $dbh->prepare($sqlUpdateWarrantyEndDate);
$sthUpdateWarrantyEndDate->bindParam(':warrantyEndDate', $warrantyEndDate, PDO::PARAM_STR);
$sthUpdateWarrantyEndDate->bindParam(':hardware_id', $hardware_id, PDO::PARAM_STR);

// bios.ssn dans la base OCS corespond au serviceTag dans le WebService DELL
$sqlSelect = "
SELECT
 a.hardware_id,
 a.".FIELD_SHIPDATE." as shipDate,
 a.".FIELD_WARRANTY_ENDDATE." as warrantyEndDate,
 bios.ssn
FROM accountinfo a LEFT JOIN bios
ON ( a.hardware_id = bios.hardware_id )
WHERE " . SELECT_WHERE_FILTER;
if (DEBUG_SQL) print_debug(__LINE__, compact('sqlSelect'));
$sthSelect = $dbh->query($sqlSelect);

/*
 * Boucle princpale avec des pages pour n'appeller
 *  le WS qu'une fois par page et pas par ligne de donnée
 */
$moreData = true;
while ( $moreData ) {
    /* Construction d'un tableau associatif avec au max WS_DELL_PAGINATE_SERVICETAGS lignes
     *  depuis les données existantes dans la DB
     */
    if (VERBOSE) print_verbose(__LINE__, "Construction d'un lot depuis la DB");
    $page = [];
    for ( $i=0; $moreData && $i<WS_DELL_PAGINATE_SERVICETAGS; $i++ ) {
        $row = $sthSelect->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            $moreData = false;
            break;
        }
        /* Nettoyer les données : sauter les machines avec un bios.ssn (ServiceTag) invalide */
        if ( ! preg_match(REGEX_SERVICETAG, $row['ssn']) ) {
            if (DEBUG_DATA) print_debug(__LINE__, $row, 'json', "Skipping invalid bios.ssn (don't match ".REGEX_SERVICETAG.")"); 
            continue;
        }
        $ssn = $row['ssn'];
        $hardware_id = $row['hardware_id'];
        $shipDateDB = $row['shipDate'];
        $warrantyEndDateDB = $row['warrantyEndDate'];
        $page[$ssn] = compact('ssn', 'hardware_id', 'shipDateDB', 'warrantyEndDateDB');
        // $page: [ 'CH921Z2' => [ hardware_id => 'string', shipDateDB => 'date', warrantyEndDateDB => 'date' ], ... ]
    }

    /* Récupérer un lot de dates depuis le WebService */
    $serviceTags = array_keys($page);
    if (VERBOSE) print_verbose(__LINE__, "Appel WS pour un lot de " . count($serviceTags));
    if (DEBUG_DATA) print_debug(__LINE__, compact('serviceTags'), 'json');
    $datesWS = getWSDates($serviceTags, $access_token);
    // $datesWS: [ 'CH921Z2' => [ shipDateWS => 'date', warrantyEndDateWS => 'date' ], ... ]
    if (DEBUG_DATA) print_debug(__LINE__, compact('datesWS'), 'json_pretty');

    /* Fusion des données de $page (venant de la DB OCS) avec les données récupérées via le WebService */
    $mergedPage = array_merge_recursive($page, $datesWS);
    // $mergedPage: [ 'CH921Z2' => [ hardware_id => 'string', shipDateDB => 'date', warrantyEndDateDB => 'date',
    //                                                        shipDateWS => 'date', warrantyEndDateWS => 'date' ], ... ]
    if (DEBUG_DATA) print_debug(__LINE__, compact('mergedPage'), 'json_pretty');

    /* Mise à jour de la base de données pour toutes les valeurs qui ont changées dans $page */
    if (VERBOSE) print_verbose(__LINE__, "Mise à jour de la DB");
    $stmtOK = 0;
    $stmtKO = 0;
    foreach ($mergedPage as $ssn => $values) {
        if ( $values['shipDateWS'] && $values['shipDateDB'] != $values['shipDateWS'] ) {
            // Mise à jour des variables liées
            $hardware_id = $values['hardware_id'];
            $shipDate = $values['shipDateWS'];
            // Execution de la requête
            if (DEBUG_SQL) print_debug(__LINE__, compact('ssn', 'hardware_id', 'shipDate'), 'json', 'Updating shipDate');
            if ( stmtExecuteOrWarning($dbh, $sthUpdateShipDate) ) {
                $stmtOK++;
            } else {
                $stmtKO++;
            }
        }
        if ( $values['warrantyEndDateWS'] && $values['warrantyEndDateDB'] != $values['warrantyEndDateWS'] ) {
            // Mise à jour des variables liées
            $hardware_id = $values['hardware_id'];
            $warrantyEndDate = $values['warrantyEndDateWS'];
            // Execution de la requête
            if (DEBUG_SQL) print_debug(__LINE__, compact('ssn', 'hardware_id', 'warrantyEndDate'), 'json', 'Updating warrantyEndDate');
            if ( stmtExecuteOrWarning($dbh, $sthUpdateWarrantyEndDate) ) {
                $stmtOK++;
            } else {
                $stmtKO++;
            }
        }
    }
    if (VERBOSE) print_verbose(__LINE__, " -> stmtOK:$stmtOK, stmtKO: $stmtKO");
}

function stmtExecuteOrWarning($dbh, $sth) {
    $res = false;
    try {
        $res = $sth->execute();
    } catch (Exception $e) {
        fwrite(STDERR, "UPDATE failed
           PDO errorInfo: ". json_encode($dbh->errorInfo()) . "
           SQL: " . $sth->queryString . "
           PDO Exception message: " . $e->getMessage()
        );
    }
    return $res;
}

function getWSDates($serviceTags, $access_token) {
    $args = "?servicetags=" . implode(',', $serviceTags);
    $setopt_array = [
        CURLOPT_URL => WS_DELL_ASSET_ENTITLEMENTS_URL.$args,
        CURLOPT_HTTPHEADER => [
            "Authorization: Bearer {$access_token}",
            "Content-Type: application/json"
        ],
        //CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POST => false,
        CURLOPT_RETURNTRANSFER => true
    ];

    /* Appeler le WebService pour un lot de servicesTags concaténés */
    $machinesWS = wsJSONGet($setopt_array);
    $datesWS = [];

    /* Pour chaque machine dans la réponse du WS */
    foreach ($machinesWS as $machine) {
        $serviceTag = $machine['serviceTag'];
        $warrantyEndDateWS = null;
        /* Déterminer la date max parmis les garanties de types pertinents */
        if ( isset( $machine['entitlements'] ) ) {
            if (DEBUG_CALC && DEBUG_CALC==$serviceTag) print_debug(__LINE__, compact('machine'), 'json_pretty');
            foreach ( $machine['entitlements'] as $e ) {
                switch ( $e['serviceLevelCode'] ) {
                case 'CB':
                case 'FL':
                    $endDate = $e['endDate'];
                    if ( isset($endDate) && is_string($endDate) && (strlen($endDate)>=10) ) {
                        $endDate = substr( $e['endDate'], 0, 10 ); 
                        // Comparaison directe de chaines de caractère ok car format de date YYYY-MM-DDT...)
                        if ( $warrantyEndDateWS < $endDate ) {
                            $warrantyEndDateWS = $endDate;
                        }
                    }
                    break;
                }
                if (DEBUG_CALC && DEBUG_CALC==$serviceTag) print_debug(__LINE__, compact('arrantyEndDateWS', 'e'), 'json_pretty');
            }
        }
        // Ne conserver que la partie date pour que la valeur soit utilisable directement en SQL
        // Exemple shipDate: "2018-04-06T00:00:00Z" (ne garder que 10 caractères, avant le T donc)
        $shipDateWS = $machine['shipDate'];
        if ( isset($shipDateWS) && is_string($shipDateWS) && (strlen($shipDateWS)>=10) ) {
            $shipDateWS = substr( $shipDateWS, 0, 10 );
        } else {
            $shipDateWS = null;
        }
        if ( !$warrantyEndDateWS ) {
            $warrantyEndDateWS = null;
        }
        if (DEBUG_CALC && DEBUG_CALC==$serviceTag) print_debug(__LINE__, compact('shipDateWS', 'warrantyEndDateWS'), 'json_pretty');

        /* Ajouter les dates de la machine courante dans le tableau $datesWS (lot de résultats) */
        $datesWS[$serviceTag] = compact('shipDateWS', 'warrantyEndDateWS');
    }
    return $datesWS;
}

function getAccessToken() {
    // TODO utiliser les fonction AUTH_BASIC de curl
    $authorization = base64_encode(WS_DELL_CLIENT_ID.':'.WS_DELL_CLIENT_SECRET);
    $setopt_array = [
        CURLOPT_URL => WS_DELL_TOKEN_URL,
        CURLOPT_HTTPHEADER => [
            "Authorization: Basic {$authorization}",
            "Content-Type: application/x-www-form-urlencoded"
        ],
        //CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => "grant_type=client_credentials"
    ];
    $result = wsJSONGet($setopt_array);
    if ( !isset( $result ) || !isset($result['access_token'] ) ) {
        throw new Exception("Impossible d'obtenir le jeton sur webservice (access_token)");
    }
    return $result['access_token'];
}
	
function wsJSONGet($setopt_array) {
    /* N'avoir qu'un seul handler curl pour tout le programme, permet le keepalive sur la connexion https */
    static $curl = null;
    if ( $curl == null ) $curl = curl_init();
    /* Reparamétrer le curl existant (attention les valeurs s'accumulent) */
    curl_setopt_array($curl, $setopt_array);
    /* Exécuter la requête REST et décoder le JSON retourné */
    $response = curl_exec($curl);
    $decoded = null;
    if ($response) {
        $decoded = json_decode($response, true);
    }
    /* Afficher du debug ou un warning selon le contenu de la réponse */
    if (DEBUG_WS) {
        $url = $setopt_array[CURLOPT_URL];
        $client_headers = $setopt_array[CURLOPT_HTTPHEADER];
        $response_first100chars = substr($response, 0, 100)."...";
        print_debug(__LINE__, compact('url', 'client_headers', 'response_first100chars', 'decoded'), 'json_pretty');
    }
    if ( !is_array($decoded) ) {
        $curl_errno = curl_errno($curl);
        $curl_error = curl_error($curl);
        throw new Exception("Pb retour WS: " . json_encode(compact('setopt_array', 'curl_errno', 'curl_error', 'response')));
    }
    return $decoded;
}

function print_verbose($line, $msg) {
    global $argv;
    fprintf(STDERR, "%s %s:%-3s %s\n", date("H:i:s"), $argv[0], $line, $msg);
}
function print_debug($line, $array, $display='print_r', $msg='') {
    global $argv;
    fprintf(STDERR, "%s %s:%-3s %s", date("H:i:s"), $argv[0], $line, $msg);
    if ( is_array($array) ) {
        switch ($display) {
        case 'print_r':
            fwrite(STDERR, " ".print_r($array, true));
            break;
        case 'json':
            fwrite(STDERR, " ".json_encode($array));
            break;
        case 'json_pretty':
            fwrite(STDERR, " ".json_encode($array, JSON_PRETTY_PRINT));
            break;
        }
    }
    fwrite(STDERR, "\n");
}
