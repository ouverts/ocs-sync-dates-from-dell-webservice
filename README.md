# ocs-sync-dates-from-dell-webservice

Ce dépôt contient un script utilisable en cron. Il met à jour les dates de livraison (shipDate) et de fin de garantie (warrantyEndDate) depuis le webservice fourni par Dell vers la base de données OCS via des champs personnalisés.

Il est recommandé d'exécuter ce script avec un compte qui n'a pas les droits root. Par exemple le compte www-data (qui peut lire ocsreports/dbconfig.inc.php).

Pour ajouter les champs dans OCS :
- Se connecter avec un compte admin à l'interface web d'OCS Reports
- Menu Gestion / Données adminsitratives
- Bouton "Nouvelle donnée"
- Remplir le formulaire
    - Données adminsitratives : ordinateurs
    - Nouveau champ : (choisir un label pertinent, par exemple DATEGARANTIE)
    - Libellé : idem (éventuellement avec des espaces ici)
    - Type de champ : DATE
    - Onglet : TAG
- Faire de même pour le second champ (DATEACHAT)
- Pour connaitre les noms de colonne SQL, se connecter à la base de données et afficher la structure de la table `accountinfo` :
```
(prod)root@vm-ocs:~/administration/ocs-sync-dates-from-dell-webservice# mysql ocsdb
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 6592
Server version: 10.5.15-MariaDB-0+deb11u1 Debian 11

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [ocsdb]> DESCRIBE accountinfo;                                                                                                                     |
+-------------+--------------+------+-----+---------+-------+
| Field       | Type         | Null | Key | Default | Extra |
+-------------+--------------+------+-----+---------+-------+
| HARDWARE_ID | int(11)      | NO   | PRI | NULL    |       |
| TAG         | varchar(255) | YES  | MUL | NA      |       |
| fields_3    | date         | YES  |     | NULL    |       |
| fields_4    | date         | YES  |     | NULL    |       |
+-------------+--------------+------+-----+---------+-------+
4 rows in set (0.002 sec)

MariaDB [ocsdb]> 
```
- Les noms des champs générés doivent être configurés dans `get_dell_date_config.php` :
```php
<?php
// [...]
// Nom des champs personnalisés via l'interface Web d'OCS et sélection des données à updater
define('FIELD_SHIPDATE', "fields_4");
define('FIELD_WARRANTY_ENDDATE', "fields_3");
```

- Le webservice Dell nécessite une authentification. Il faut l'activer et configurer l'`ID` et le `SECRET` dans le même fichier de configuration :
```php
<?php
// [...]
// Webservice DELL
define('WS_DELL_CLIENT_ID', "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
define('WS_DELL_CLIENT_SECRET', "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
define('WS_DELL_TOKEN_URL', "https://apigtwb2c.us.dell.com/auth/oauth/v2/token");
```
- On peut lancer le script directement en argument de la commande `php` pour le tester manuellement :
```
(prod)root@vm-ocs:~/administration# su - www-data -s /bin/bash
(prod)www-data@vm-ocs:~$ cd /var/www/ocs
(prod)www-data@vm-ocs:~/ocs$ ls
get_dell_date_config.php  get_dell_date_v2.php
(prod)www-data@vm-ocs:~/ocs$ php get_dell_date_v2.php
16:40:48 get_dell_date_v2.php:16  Demande access_token au webservice
16:40:48 get_dell_date_v2.php:22  Connexion à la base mysql:host=localhost;port=3306;dbname=ocsdb
16:40:48 get_dell_date_v2.php:72  Construction d'un lot depuis la DB
16:40:48 get_dell_date_v2.php:95  Appel WS pour un lot de 100
16:40:49 get_dell_date_v2.php:107 Mise à jour de la DB
16:40:49 get_dell_date_v2.php:136  -> stmtOK:0, stmtKO: 0
16:40:49 get_dell_date_v2.php:72  Construction d'un lot depuis la DB
16:40:49 get_dell_date_v2.php:95  Appel WS pour un lot de 98
16:40:49 get_dell_date_v2.php:107 Mise à jour de la DB
16:40:49 get_dell_date_v2.php:136  -> stmtOK:0, stmtKO: 0
16:40:49 get_dell_date_v2.php:72  Construction d'un lot depuis la DB
16:40:49 get_dell_date_v2.php:95  Appel WS pour un lot de 100
16:40:49 get_dell_date_v2.php:107 Mise à jour de la DB
16:40:49 get_dell_date_v2.php:136  -> stmtOK:0, stmtKO: 0
16:40:49 get_dell_date_v2.php:72  Construction d'un lot depuis la DB
16:40:49 get_dell_date_v2.php:95  Appel WS pour un lot de 97
16:40:50 get_dell_date_v2.php:107 Mise à jour de la DB
16:40:50 get_dell_date_v2.php:136  -> stmtOK:0, stmtKO: 0
16:40:50 get_dell_date_v2.php:72  Construction d'un lot depuis la DB
16:40:50 get_dell_date_v2.php:95  Appel WS pour un lot de 94
16:40:50 get_dell_date_v2.php:107 Mise à jour de la DB
16:40:50 get_dell_date_v2.php:136  -> stmtOK:0, stmtKO: 0
16:40:50 get_dell_date_v2.php:72  Construction d'un lot depuis la DB
16:40:50 get_dell_date_v2.php:95  Appel WS pour un lot de 100
16:40:50 get_dell_date_v2.php:107 Mise à jour de la DB
16:40:50 get_dell_date_v2.php:136  -> stmtOK:0, stmtKO: 0
16:40:50 get_dell_date_v2.php:72  Construction d'un lot depuis la DB
16:40:50 get_dell_date_v2.php:95  Appel WS pour un lot de 94
16:40:51 get_dell_date_v2.php:107 Mise à jour de la DB
16:40:51 get_dell_date_v2.php:136  -> stmtOK:0, stmtKO: 0
(prod)www-data@vm-ocs:~/ocs$
```
